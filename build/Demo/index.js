(function(){
      
  var createPageHandler = function() {
    return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	var $app_template$ = __webpack_require__(4)
	var $app_style$ = __webpack_require__(5)
	var $app_script$ = __webpack_require__(6)
	
	$app_define$('@app-component/index', [], function($app_require$, $app_exports$, $app_module$){
	     $app_script$($app_module$, $app_exports$, $app_require$)
	     if ($app_exports$.__esModule && $app_exports$.default) {
	            $app_module$.exports = $app_exports$.default
	        }
	     $app_module$.exports.template = $app_template$
	     $app_module$.exports.style = $app_style$
	})
	
	$app_bootstrap$('@app-component/index',{ packagerVersion: '0.0.5'})


/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, exports) {

	module.exports = {
	  "type": "div",
	  "attr": {},
	  "children": [
	    {
	      "type": "div",
	      "attr": {
	        "show": function () {return this.isShowDialog}
	      },
	      "classList": [
	        "progressDiv"
	      ],
	      "children": [
	        {
	          "type": "progress",
	          "attr": {
	            "type": "circular"
	          },
	          "classList": [
	            "circular"
	          ],
	          "style": {
	            "width": "60px",
	            "height": "60px",
	            "color": "#F0FFF0"
	          }
	        },
	        {
	          "type": "text",
	          "attr": {
	            "value": "加载中..."
	          },
	          "style": {
	            "color": "#FFFFFF",
	            "marginTop": "8px"
	          }
	        }
	      ]
	    },
	    {
	      "type": "div",
	      "attr": {},
	      "classList": [
	        "demo-page"
	      ],
	      "children": [
	        {
	          "type": "div",
	          "attr": {},
	          "classList": [
	            "col"
	          ],
	          "style": {
	            "width": "100%",
	            "backgroundColor": "#ffffff",
	            "paddingTop": "30px",
	            "paddingBottom": "20px"
	          },
	          "children": [
	            {
	              "type": "text",
	              "attr": {
	                "value": function () {return this.currentUser.name}
	              },
	              "classList": [
	                "title"
	              ],
	              "style": {
	                "marginLeft": "40px"
	              }
	            },
	            {
	              "type": "div",
	              "attr": {},
	              "classList": [
	                "title"
	              ],
	              "style": {
	                "position": "fixed",
	                "top": "32px",
	                "right": "24px",
	                "display": "flex",
	                "justifyContent": "flex-end"
	              },
	              "children": [
	                {
	                  "type": "text",
	                  "attr": {
	                    "value": " 详情 "
	                  },
	                  "events": {
	                    "click": function (evt) {this.goDetail(evt)}
	                  },
	                  "style": {
	                    "right": "10px",
	                    "backgroundColor": "#2a8cec",
	                    "color": "#FFFFFF",
	                    "paddingTop": "3px",
	                    "paddingRight": "8px",
	                    "paddingBottom": "3px",
	                    "paddingLeft": "8px",
	                    "borderRadius": "8px"
	                  }
	                }
	              ]
	            },
	            {
	              "type": "text",
	              "attr": {
	                "value": function () {return this.nowDate}
	              },
	              "classList": [
	                "title"
	              ],
	              "style": {
	                "marginLeft": "40px"
	              }
	            }
	          ]
	        },
	        {
	          "type": "block",
	          "attr": {},
	          "shown": function () {return this.signBean.address==''&&this.signBean.signOutAddress==''},
	          "children": [
	            {
	              "type": "div",
	              "attr": {},
	              "classList": [
	                "row"
	              ],
	              "children": [
	                {
	                  "type": "div",
	                  "attr": {},
	                  "classList": [
	                    "center"
	                  ],
	                  "style": {
	                    "width": "80px",
	                    "marginTop": "80px"
	                  },
	                  "children": [
	                    {
	                      "type": "div",
	                      "attr": {},
	                      "classList": [
	                        "v-line"
	                      ]
	                    },
	                    {
	                      "type": "div",
	                      "attr": {},
	                      "classList": [
	                        "blue-dot"
	                      ]
	                    }
	                  ]
	                },
	                {
	                  "type": "div",
	                  "attr": {},
	                  "classList": [
	                    "bgSign"
	                  ],
	                  "style": {
	                    "backgroundColor": "#cde2f6"
	                  },
	                  "children": [
	                    {
	                      "type": "text",
	                      "attr": {
	                        "value": " 今天还未签到... "
	                      },
	                      "classList": [
	                        "signDate"
	                      ],
	                      "style": {
	                        "fontSize": "32px",
	                        "color": "#555555",
	                        "fontWeight": "normal"
	                      }
	                    }
	                  ]
	                }
	              ]
	            }
	          ]
	        },
	        {
	          "type": "block",
	          "attr": {},
	          "shown": function () {return (this.signBean.address!='')&&!(this.signBean.address==''&&this.signBean.signOutAddress=='')},
	          "children": [
	            {
	              "type": "div",
	              "attr": {},
	              "classList": [
	                "row"
	              ],
	              "children": [
	                {
	                  "type": "div",
	                  "attr": {},
	                  "classList": [
	                    "center"
	                  ],
	                  "style": {
	                    "width": "80px",
	                    "marginTop": "80px"
	                  },
	                  "children": [
	                    {
	                      "type": "div",
	                      "attr": {},
	                      "classList": [
	                        "v-line"
	                      ]
	                    },
	                    {
	                      "type": "div",
	                      "attr": {},
	                      "classList": [
	                        "blue-dot"
	                      ]
	                    }
	                  ]
	                },
	                {
	                  "type": "div",
	                  "attr": {},
	                  "classList": [
	                    "bgSign"
	                  ],
	                  "children": [
	                    {
	                      "type": "div",
	                      "attr": {},
	                      "classList": [
	                        "row"
	                      ],
	                      "children": [
	                        {
	                          "type": "text",
	                          "attr": {
	                            "value": function () {return this.signBean.createTime.substring(10,16)}
	                          },
	                          "classList": [
	                            "signDate"
	                          ],
	                          "style": {
	                            "fontSize": "40px",
	                            "fontWeight": "normal"
	                          }
	                        },
	                        {
	                          "type": "text",
	                          "attr": {
	                            "value": " 签到 "
	                          },
	                          "classList": [
	                            "signDate"
	                          ],
	                          "style": {
	                            "marginLeft": "30px",
	                            "marginBottom": "-10px"
	                          }
	                        },
	                        {
	                          "type": "div",
	                          "attr": {},
	                          "style": {
	                            "flex": 1,
	                            "justifyContent": "flex-end",
	                            "marginLeft": "30px",
	                            "marginBottom": "-10px"
	                          },
	                          "children": [
	                            {
	                              "type": "text",
	                              "attr": {
	                                "value": "撤销 "
	                              },
	                              "classList": [
	                                "signDate"
	                              ],
	                              "style": {
	                                "textDecoration": "underline"
	                              },
	                              "events": {
	                                "click": function (evt) {this.showRevertDialog('1',evt)}
	                              },
	                              "shown": function () {return this.signBean.signOutAddress==''}
	                            }
	                          ]
	                        }
	                      ]
	                    },
	                    {
	                      "type": "text",
	                      "attr": {
	                        "value": function () {return this.signBean.address}
	                      },
	                      "classList": [
	                        "signDate"
	                      ]
	                    },
	                    {
	                      "type": "text",
	                      "attr": {
	                        "value": function () {return this.currentUser.companyName}
	                      },
	                      "classList": [
	                        "signDate"
	                      ]
	                    },
	                    {
	                      "type": "image",
	                      "attr": {
	                        "id": "id_signInImage",
	                        "src": function () {return this.signBean.signUrl},
	                        "alt": "../Common/img/ic_default_image.png"
	                      },
	                      "id": "id_signInImage",
	                      "events": {
	                        "click": function (evt) {this.lookImage(this.signBean.signUrl,evt)}
	                      },
	                      "classList": [
	                        "signImg"
	                      ]
	                    }
	                  ]
	                }
	              ]
	            }
	          ]
	        },
	        {
	          "type": "block",
	          "attr": {},
	          "shown": function () {return this.signBean.signOutAddress!=''},
	          "children": [
	            {
	              "type": "div",
	              "attr": {},
	              "classList": [
	                "row"
	              ],
	              "children": [
	                {
	                  "type": "div",
	                  "attr": {},
	                  "classList": [
	                    "center"
	                  ],
	                  "style": {
	                    "width": "80px",
	                    "marginTop": "80px"
	                  },
	                  "children": [
	                    {
	                      "type": "div",
	                      "attr": {},
	                      "classList": [
	                        "v-line"
	                      ]
	                    },
	                    {
	                      "type": "div",
	                      "attr": {},
	                      "classList": [
	                        "blue-dot"
	                      ]
	                    }
	                  ]
	                },
	                {
	                  "type": "div",
	                  "attr": {},
	                  "classList": [
	                    "bgSign"
	                  ],
	                  "children": [
	                    {
	                      "type": "div",
	                      "attr": {},
	                      "classList": [
	                        "row"
	                      ],
	                      "children": [
	                        {
	                          "type": "text",
	                          "attr": {
	                            "value": function () {return this.signBean.signOutTime.substring(10,16)}
	                          },
	                          "classList": [
	                            "signDate"
	                          ],
	                          "style": {
	                            "fontSize": "40px",
	                            "fontWeight": "normal"
	                          }
	                        },
	                        {
	                          "type": "text",
	                          "attr": {
	                            "value": " 签退 "
	                          },
	                          "classList": [
	                            "signDate"
	                          ],
	                          "style": {
	                            "marginLeft": "30px",
	                            "marginBottom": "-10px"
	                          }
	                        },
	                        {
	                          "type": "div",
	                          "attr": {},
	                          "style": {
	                            "flex": 1,
	                            "justifyContent": "flex-end",
	                            "marginLeft": "30px",
	                            "marginBottom": "-10px"
	                          },
	                          "children": [
	                            {
	                              "type": "text",
	                              "attr": {
	                                "value": "撤销 "
	                              },
	                              "classList": [
	                                "signDate"
	                              ],
	                              "style": {
	                                "textDecoration": "underline"
	                              },
	                              "events": {
	                                "click": function (evt) {this.showRevertDialog('2',evt)}
	                              },
	                              "shown": function () {return this.signBean.signOutAddress!=''}
	                            }
	                          ]
	                        }
	                      ]
	                    },
	                    {
	                      "type": "text",
	                      "attr": {
	                        "value": function () {return this.signBean.signOutAddress}
	                      },
	                      "classList": [
	                        "signDate"
	                      ]
	                    },
	                    {
	                      "type": "text",
	                      "attr": {
	                        "value": function () {return this.currentUser.companyName}
	                      },
	                      "classList": [
	                        "signDate"
	                      ]
	                    },
	                    {
	                      "type": "image",
	                      "attr": {
	                        "id": "id_signOutImage",
	                        "alt": "../Common/img/ic_default_image.png",
	                        "src": function () {return this.signBean.signOutUrl}
	                      },
	                      "id": "id_signOutImage",
	                      "classList": [
	                        "signImg"
	                      ],
	                      "events": {
	                        "click": function (evt) {this.lookImage(this.signBean.signOutUrl,evt)}
	                      }
	                    }
	                  ]
	                }
	              ]
	            }
	          ]
	        }
	      ]
	    },
	    {
	      "type": "div",
	      "attr": {},
	      "style": {
	        "position": "fixed",
	        "height": "180px",
	        "bottom": "50px",
	        "left": "285px"
	      },
	      "children": [
	        {
	          "type": "div",
	          "attr": {},
	          "shown": function () {return this.signBean.address==''},
	          "classList": function () {return [this.btnBgStyle]},
	          "events": {
	            "click": function (evt) {this.takePic(evt)}
	          },
	          "children": [
	            {
	              "type": "text",
	              "attr": {
	                "value": function () {return this.signText}
	              },
	              "style": {
	                "width": "100%",
	                "color": "#ffffff",
	                "textAlign": "center",
	                "fontSize": "36px"
	              }
	            }
	          ]
	        },
	        {
	          "type": "div",
	          "attr": {},
	          "shown": function () {return (this.signBean.signOutAddress=='')&&!(this.signBean.address=='')},
	          "classList": function () {return [this.btnBgStyle]},
	          "events": {
	            "click": function (evt) {this.takePic(evt)}
	          },
	          "children": [
	            {
	              "type": "text",
	              "attr": {
	                "id": "t2",
	                "value": function () {return this.signText}
	              },
	              "id": "t2",
	              "classList": function () {return [this.blink]},
	              "style": {
	                "width": "100%",
	                "color": "#ffffff",
	                "textAlign": "center",
	                "fontSize": "36px"
	              }
	            }
	          ]
	        }
	      ]
	    }
	  ]
	}

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	module.exports = {
	  ".doc-page": {
	    "flex": 1,
	    "flexDirection": "column"
	  },
	  ".page-title-wrap": {
	    "paddingTop": "50px",
	    "paddingBottom": "80px",
	    "justifyContent": "center"
	  },
	  ".page-title": {
	    "paddingTop": "30px",
	    "paddingBottom": "30px",
	    "paddingLeft": "40px",
	    "paddingRight": "40px",
	    "borderTopColor": "#bbbbbb",
	    "borderRightColor": "#bbbbbb",
	    "borderBottomColor": "#bbbbbb",
	    "borderLeftColor": "#bbbbbb",
	    "color": "#bbbbbb",
	    "borderBottomWidth": "2px"
	  },
	  ".btn": {
	    "height": "86px",
	    "textAlign": "center",
	    "borderRadius": "43px",
	    "marginRight": "60px",
	    "marginLeft": "60px",
	    "marginBottom": "50px",
	    "color": "#ffffff",
	    "fontSize": "30px",
	    "backgroundColor": "#09ba07",
	    "width": "550px",
	    "marginTop": "75px"
	  },
	  ".row": {
	    "display": "flex",
	    "flexDirection": "row"
	  },
	  ".col": {
	    "display": "flex",
	    "flexDirection": "column"
	  },
	  ".progressDiv": {
	    "position": "fixed",
	    "top": "350px",
	    "left": "280px",
	    "flexDirection": "column",
	    "justifyContent": "center",
	    "alignItems": "center",
	    "textAlign": "center",
	    "backgroundColor": "#6d6e6a",
	    "borderRadius": "20px",
	    "width": "200px",
	    "height": "200px",
	    "zIndex": 9999
	  },
	  ".demo-page": {
	    "flexDirection": "column",
	    "justifyContent": "flex-start",
	    "alignItems": "center",
	    "backgroundColor": "#eeeeee"
	  },
	  ".center": {
	    "justifyContent": "center",
	    "alignItems": "center"
	  },
	  ".title": {
	    "fontSize": "32px",
	    "paddingTop": "5px",
	    "paddingRight": "5px",
	    "paddingBottom": "5px",
	    "paddingLeft": "5px",
	    "color": "#333333"
	  },
	  ".bgSign": {
	    "width": "82%",
	    "marginRight": "50px",
	    "backgroundColor": "#318ee9",
	    "paddingTop": "20px",
	    "paddingRight": "20px",
	    "paddingBottom": "20px",
	    "paddingLeft": "20px",
	    "borderRadius": "25px",
	    "flexDirection": "column",
	    "marginTop": "50px",
	    "marginLeft": "10px"
	  },
	  ".signDate": {
	    "fontSize": "26px",
	    "color": "#ffffff",
	    "marginTop": "10px"
	  },
	  ".signImg": {
	    "width": "130px",
	    "height": "130px",
	    "marginTop": "30px"
	  },
	  ".v-line": {
	    "height": "300px",
	    "width": "1px",
	    "backgroundColor": "#333333"
	  },
	  ".blue-dot": {
	    "width": "12px",
	    "height": "12px",
	    "backgroundColor": "#2c8cec",
	    "borderRadius": "6px",
	    "marginLeft": "-6px"
	  },
	  ".btnSign": {
	    "width": "180px",
	    "height": "180px",
	    "backgroundColor": "#2c8cec",
	    "borderRadius": "90px",
	    "borderTopWidth": "10px",
	    "borderRightWidth": "10px",
	    "borderBottomWidth": "10px",
	    "borderLeftWidth": "10px",
	    "borderTopColor": "#cde2f6",
	    "borderRightColor": "#cde2f6",
	    "borderBottomColor": "#cde2f6",
	    "borderLeftColor": "#cde2f6"
	  },
	  ".btnSignFalse": {
	    "width": "180px",
	    "height": "180px",
	    "backgroundColor": "#bdbdbd",
	    "borderRadius": "90px",
	    "borderTopWidth": "10px",
	    "borderRightWidth": "10px",
	    "borderBottomWidth": "10px",
	    "borderLeftWidth": "10px",
	    "borderTopColor": "#dcdcdc",
	    "borderRightColor": "#dcdcdc",
	    "borderBottomColor": "#dcdcdc",
	    "borderLeftColor": "#dcdcdc"
	  },
	  ".blink": {
	    "animationName": "Opacity",
	    "animationDuration": "1200ms",
	    "animationIterationCount": -1,
	    "animationTimingFunction": "ease-in-out"
	  },
	  "@KEYFRAMES": {
	    "Opacity": [
	      {
	        "opacity": 1,
	        "time": 0
	      },
	      {
	        "opacity": 0.2,
	        "time": 100
	      }
	    ]
	  }
	}

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = function(module, exports, $app_require$){'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _system = $app_require$('@app-module/system.router');
	
	var _system2 = _interopRequireDefault(_system);
	
	var _system3 = $app_require$('@app-module/system.storage');
	
	var _system4 = _interopRequireDefault(_system3);
	
	var _system5 = $app_require$('@app-module/system.geolocation');
	
	var _system6 = _interopRequireDefault(_system5);
	
	var _system7 = $app_require$('@app-module/system.image');
	
	var _system8 = _interopRequireDefault(_system7);
	
	var _system9 = $app_require$('@app-module/system.request');
	
	var _system10 = _interopRequireDefault(_system9);
	
	var _system11 = $app_require$('@app-module/system.media');
	
	var _system12 = _interopRequireDefault(_system11);
	
	var _common = __webpack_require__(7);
	
	var _UrlConfig = __webpack_require__(8);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var prompt = $app_require$('@app-module/system.prompt');
	exports.default = {
	    data: function data() {
	        return {
	            currentUser: {},
	            nowDate: '',
	            signBean: {
	                address: '',
	                am: '',
	                createTime: '',
	                createUserId: '',
	                id: '',
	                latitude: '',
	                longitude: '',
	                pm: '',
	                remark: '',
	                signDate: '',
	                signOutAddress: '',
	                signOutLatitude: '',
	                signOutLongitude: '',
	                signOutTime: '',
	                signOutUrl: '',
	                signUrl: '',
	                userId: ''
	            },
	            lat: '',
	            lng: '',
	            locAddress: '',
	            signText: '定位中',
	            btnBgStyle: 'btnSignFalse',
	            isShowDialog: false,
	            blink: 'blink'
	        };
	    },
	    onInit: function onInit() {
	        this.$page.setTitleBar({ text: '考勤' });
	        this.nowDate = this.getNowFormatDate() + '  ' + this.getWeek(this.getNowFormatDate());
	        var that = this;
	        _system4.default.get({
	            key: 'userInfo',
	            success: function success(userInfo) {
	                that.currentUser = JSON.parse(userInfo);
	                that.getTodaySign();
	            },
	            fail: function fail(data, code) {
	                console.log("handling fail, code=" + code);
	            }
	        });
	        that.getParam();
	    },
	    takePic: function takePic() {
	        var that = this;
	        if (that.signText == '重试') {
	            that.getParam();
	            return;
	        } else if (that.signText == '定位中') {
	            prompt.showToast({
	                message: '\u6B63\u5728\u5B9A\u4F4D\u4E2D\uFF0C\u8BF7\u7A0D\u540E'
	            });
	            return;
	        }
	        _system12.default.takePhoto({
	            success: function success(data) {
	                console.log('\u62CD\u7167\u6210\u529F: ' + data.uri);
	
	                that.compressImage(data.uri);
	            }
	        });
	    },
	    getNowFormatDate: function getNowFormatDate() {
	        var date = new Date();
	        var seperator1 = "-";
	        var year = date.getFullYear();
	        var month = date.getMonth() + 1;
	        var strDate = date.getDate();
	        if (month >= 1 && month <= 9) {
	            month = "0" + month;
	        }
	        if (strDate >= 0 && strDate <= 9) {
	            strDate = "0" + strDate;
	        }
	        var currentdate = year + seperator1 + month + seperator1 + strDate;
	        return currentdate;
	    },
	    compressImage: function compressImage(uri) {
	        var that = this;
	        _system8.default.compressImage({
	            uri: uri,
	            quality: 30,
	            format: 'JPEG',
	            success: function success(data) {
	                console.log('\u538B\u7F29\u540E: ' + data.uri);
	
	                that.uploadImage(data.uri);
	            },
	            fail: function fail(data, code) {
	                console.log('handling fail, code = ' + code);
	            }
	        });
	    },
	    uploadImage: function uploadImage(uri) {
	        var that = this;
	        _system10.default.upload({
	            url: _UrlConfig.URL.uploadFile,
	            files: [{
	                uri: uri,
	                name: 'file',
	                filename: 'test.jpg'
	            }],
	            data: [{
	                name: 'fileType',
	                value: '1'
	            }],
	            success: function success(res) {
	                var imgUrl = JSON.parse(res.data).url;
	
	                if (that.signBean.address == '') {
	                    that.signBean.signUrl = imgUrl;
	                    that.goSignIn();
	                } else {
	                    that.signBean.signOutUrl = imgUrl;
	                    that.goSignOut();
	                }
	            },
	            fail: function fail(msg, code) {
	                prompt.showToast({
	                    message: '\u4E0A\u4F20\u5931\u8D25:' + (code + ': ' + msg)
	                });
	            }
	        });
	    },
	    getParam: function getParam() {
	        var that = this;
	        _system6.default.getLocation({
	            success: function success(data) {
	                that.getBaiDuPoi(data.latitude, data.longitude);
	            },
	            fail: function fail(data, code) {
	                prompt.showToast({
	                    message: '\u5B9A\u4F4D\u5931\u8D25:' + ('code = ' + code + ', \u6B63\u5728\u91CD\u8BD5')
	                });
	                that.getParam();
	            }
	        });
	    },
	    getBaiDuPoi: function getBaiDuPoi(lat, lng) {
	        var that = this;
	        _common.http.GET(_UrlConfig.URL.getAddress + (lat + ',' + lng), "", function (data) {
	            var res = JSON.parse(data);
	            that.lat = lat;
	            that.lng = lng;
	            that.locAddress = res.result.formatted_address;
	            that.btnBgStyle = 'btnSign';
	            that.blink = '';
	            if (that.signBean.address == '') {
	                that.signText = '签到';
	            } else if (that.signBean.signOutAddress == '') {
	                that.signText = '签退';
	            } else {
	                that.signText = '重试';
	            }
	        });
	    },
	    goSignIn: function goSignIn() {
	        var that = this;
	        that.showDialog();
	        var param = {
	            id: '',
	            userId: that.currentUser.id,
	            address: that.locAddress,
	            longitude: that.lng,
	            latitude: that.lat,
	            signUrl: that.signBean.signUrl,
	            remark: ''
	        };
	        _common.http.postMethod(_UrlConfig.URL.signIn, param, function (sendData) {
	            that.getTodaySign();
	        });
	    },
	    goSignOut: function goSignOut() {
	        var that = this;
	        that.showDialog();
	        var param = {
	            id: that.signBean.id,
	            userId: that.currentUser.id,
	            signOutAddress: that.locAddress,
	            signOutLongitude: that.lng,
	            signOutLatitude: that.lat,
	            signOutUrl: that.signBean.signOutUrl,
	            remark: ''
	        };
	        _common.http.postMethod(_UrlConfig.URL.signIn, param, function (sendData) {
	            that.getTodaySign();
	        });
	    },
	    showRevertDialog: function showRevertDialog(type) {
	        var that = this;
	        var tip = type == '1' ? '签到' : '签退';
	        prompt.showDialog({
	            title: '提示',
	            message: '是否要撤销本次' + tip + '信息？',
	            buttons: [{
	                text: '确定',
	                color: '#2c8cec'
	            }, {
	                text: '取消',
	                color: '#999999'
	            }],
	            success: function success(data) {
	                var i = data.index;
	                if (i == 0) {
	                    that.revertSign(type);
	                }
	            }
	        });
	    },
	    revertSign: function revertSign(type) {
	        var that = this;
	        that.showDialog();
	        var url = '';
	        if (type == '1') {
	            url = _UrlConfig.URL.revertIn;
	        } else {
	            url = _UrlConfig.URL.revertOut;
	        }
	        var param = {
	            id: that.signBean.id
	        };
	        _common.http.postMethod(url, param, function (sendData) {
	            prompt.showToast({
	                message: '\u64A4\u9500\u6210\u529F'
	            });
	            that.getTodaySign();
	        });
	    },
	    getTodaySign: function getTodaySign() {
	        var that = this;
	        that.showDialog();
	        var param = {
	            userId: that.currentUser.id,
	            signDate: that.getNowFormatDate()
	        };
	        _common.http.postMethod(_UrlConfig.URL.getSign, param, function (sendData) {
	            console.log(sendData);
	            that.hideDialog();
	            if (sendData) {
	                that.signBean = sendData;
	                if (that.locAddress != '' && that.signBean.address != '' && that.signBean.signOutAddress == '') {
	                    that.signText = '签退';
	                }
	            } else {
	                that.signBean.address = '';
	                that.signBean.signOutAddress = '';
	                if (that.locAddress != '' && that.signBean.address == '') {
	                    that.signText = '签到';
	                }
	            }
	        });
	    },
	    lookImage: function lookImage(imgUrl, evt) {
	        console.info('\u89E6\u53D1\u4E8B\u4EF6\uFF1A\u7C7B\u578B\uFF1A' + evt.type + ', \u8BE6\u60C5\uFF1A ' + JSON.stringify(evt.detail));
	        console.log("查看图片:" + imgUrl);
	        _system2.default.push({
	            uri: '/ShowImage',
	            params: { imageUrl: imgUrl }
	        });
	    },
	    goDetail: function goDetail() {
	        _system2.default.push({
	            uri: '/About'
	        });
	    },
	    getWeek: function getWeek(dateString) {
	        var date;
	        if (this.isNull(dateString)) {
	            date = new Date();
	        } else {
	            var dateArray = dateString.split("-");
	            date = new Date(dateArray[0], parseInt(dateArray[1] - 1), dateArray[2]);
	        }
	        return "星期" + "日一二三四五六".charAt(date.getDay());
	    },
	    isNull: function isNull(object) {
	        if (object == null || typeof object == "undefined") {
	            return true;
	        }
	        return false;
	    },
	    showDialog: function showDialog() {
	        this.isShowDialog = true;
	    },
	    hideDialog: function hideDialog() {
	        var that = this;
	        setTimeout(function () {
	            that.isShowDialog = false;
	        }, 800);
	    }
	};
	
	
	var moduleOwn = exports.default || module.exports;
	var accessors = ['public', 'protected', 'private'];
	
	if (moduleOwn.data && accessors.some(function (acc) {
	    return moduleOwn[acc];
	})) {
	    throw new Error('页面VM对象中的属性data不可与"' + accessors.join(',') + '"同时存在，请使用private替换data名称');
	} else if (!moduleOwn.data) {
	    moduleOwn.data = {};
	    moduleOwn._descriptor = {};
	    accessors.forEach(function (acc) {
	        var accType = _typeof(moduleOwn[acc]);
	        if (accType === 'object') {
	            moduleOwn.data = Object.assign(moduleOwn.data, moduleOwn[acc]);
	            for (var name in moduleOwn[acc]) {
	                moduleOwn._descriptor[name] = { access: acc };
	            }
	        } else if (accType === 'function') {
	            console.warn('页面VM对象中的属性' + acc + '的值不能是函数，请使用对象');
	        }
	    });
	}}

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var fetch = $app_require$('@app-module/system.fetch');
	var prompt = $app_require$('@app-module/system.prompt');
	
	var http = exports.http = {
	    GET: function GET(url, param, callback) {
	        fetch.fetch({
	            url: url,
	            method: 'GET',
	            data: param,
	            success: function success(res) {
	                console.log(res.data);
	                callback(res.data);
	            },
	            fail: function fail(res, code) {
	                console.log(url + ":\n" + res);
	                callback(res);
	            }
	        });
	    },
	
	    getMethod: function getMethod(url, param, callback) {
	        fetch.fetch({
	            url: url,
	            method: 'GET',
	            data: JSON.stringify(param),
	            contentType: 'application/json',
	            success: function success(res) {
	                console.log(JSON.stringify(res.data.sendData));
	                callback(res.data.sendData);
	            },
	            fail: function fail(res, code) {
	                console.log(url + ":\n" + res);
	                callback(res.data.sendDate);
	            }
	        });
	    },
	
	    postMethod: function postMethod(url, param, callback) {
	        console.log('[POST] ' + url);
	        console.log('[PARAM]  ' + JSON.stringify(param));
	        fetch.fetch({
	            url: url,
	            method: 'POST',
	            header: { 'Content-Type': 'application/json;charset=UTF-8' },
	            data: JSON.stringify(param),
	            success: function success(res) {
	                console.log('[CALLBACK] ' + res.data);
	                var obj = JSON.parse(res.data);
	                if (obj.e.code === 0) {
	                    callback(obj.data.sendData);
	                } else {
	                    prompt.showToast({
	                        message: obj.e.desc
	                    });
	                }
	            },
	            fail: function fail(res, code) {
	                console.log(res + " | " + "code= " + code);
	                callback(res);
	            }
	        });
	    }
	};

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var IP = "http://oaapi.yingfeng365.com/jsyf-oa";
	var URL = exports.URL = {
	    //登录
	    loginIn: IP + "/user/login.json",
	
	    //签到
	    signIn: IP + "/signIn/save.json",
	
	    //签到撤销
	    revertIn: IP + "/signIn/revertIn.json",
	
	    //签退撤销
	    revertOut: IP + "/signIn/revert.json",
	
	    //百度逆地址解析
	    getAddress: "http://api.map.baidu.com/geocoder/v2/?callback=&output=json&pois=0&ak=18eZsB5LvSoIf1HZOCUBDXKWHMbvhfpa&location=",
	
	    //查询签到
	    getSign: IP + "/signIn/getSignInByDay.json",
	
	    //上传文件
	    uploadFile: IP + "/attach/uploadFile.json",
	
	    //获取推荐
	    getRecommend: "https://www.apiopen.top/satinApi",
	
	    //获取美图
	    getPic: "https://www.apiopen.top/meituApi"
	
	};

/***/ })
/******/ ]);
  };
  if (typeof window === "undefined") {
    return createPageHandler();
  }
  else {
    window.createPageHandler = createPageHandler
  }
})();
//# sourceMappingURL=index.js.map